package com.achshar.techstreet;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Settings extends TechStreet.Main {

    TextInputLayout oldPasswordWrapper;
    TextInputLayout newPasswordWrapper;

    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_settings);

        oldPasswordWrapper = (TextInputLayout) findViewById(R.id.oldPasswordWrapper);
        oldPasswordWrapper.setHint(getResources().getString(R.string.settingsLabelOldPassword));
        oldPasswordWrapper.setErrorEnabled(true);

        newPasswordWrapper = (TextInputLayout) findViewById(R.id.newPasswordWrapper);
        newPasswordWrapper.setHint(getResources().getString(R.string.settingsLabelNewPassword));
        newPasswordWrapper.setErrorEnabled(true);

        super.onCreate(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.settings, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                saveSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveSettings() {

        String oldPassword = oldPasswordWrapper.getEditText().getText().toString();
        String newPassword = newPasswordWrapper.getEditText().getText().toString();

        oldPasswordWrapper.setError(null);
        newPasswordWrapper.setError(null);

        if(oldPassword.isEmpty() || newPassword.isEmpty()) {

            if(oldPassword.isEmpty())
                oldPasswordWrapper.setError("Old Password is required!");

            if(newPassword.isEmpty())
                newPasswordWrapper.setError("New Password is required!");

            return;
        }

        HashMap<String, String> GETParams = new HashMap<>();
        GETParams.put("action", "changepassword");

        HashMap<String, String> POSTParams = new HashMap<>();
        POSTParams.put("oldpassword", oldPassword);
        POSTParams.put("newpassword", newPassword);

        new API.Call("profile.php", GETParams, POSTParams, new saveAttempted(this));
    }

    class saveAttempted extends API.Callback {

        saveAttempted(Activity context) {
            super(context);
        }

        public void method(JSONObject response) throws JSONException {

            if(response.optBoolean("status", false)) {
                Snackbar.make(context.findViewById(R.id.container), "Settings Updated", Snackbar.LENGTH_LONG).show();
                return;
            }

            TextInputLayout ErrorWrapper = null;

            if(response.optString("response").equals("Invalid Password!"))
                ErrorWrapper = oldPasswordWrapper;

            if(ErrorWrapper == null)
                Toast.makeText(context.getApplicationContext(), "Error Saving Settings", Toast.LENGTH_LONG).show();

            else
                ErrorWrapper.setError(response.optString("response"));
        }
    }
}
