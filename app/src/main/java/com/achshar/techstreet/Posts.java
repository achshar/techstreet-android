package com.achshar.techstreet;

import android.app.Activity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Posts extends TechStreet.Main {

    String username = null;
    String orderBy = "votes";
    String order = "desc";
    Integer start = 0;
    Integer count = 20;

    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_posts);
        super.onCreate(savedInstanceState);

        loadPosts();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.posts, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.sortTime:
                orderBy = "time";
                break;
            case R.id.sortVotes:
                orderBy = "Votes";
                break;
            case R.id.orderAsc:
                order = "asc";
                break;
            case R.id.orderDesc:
                order = "desc";
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        loadPosts();
        return true;
    }

    private void loadPosts() {

        HashMap<String, String> GETParams = new HashMap<>();

        GETParams.put("action", "get");
        GETParams.put("orderby", orderBy);
        GETParams.put("order", order);
        GETParams.put("start", start.toString());
        GETParams.put("count", count.toString());
        if(username != null)
            GETParams.put("username", username);

        HashMap<String, String> POSTParams = new HashMap<>();

        new API.Call("posts.php", GETParams, POSTParams, new PostsLoaded(this));
    }

    class PostsLoaded extends API.Callback {

        PostsLoaded(Activity context) {
            super(context);
        }

        public void method(JSONObject response) throws JSONException {

            if(!response.optBoolean("status", false)) {
                Toast.makeText(context.getApplicationContext(), "Error Loading Posts", Toast.LENGTH_LONG).show();
                return;
            }

            JSONArray jPosts = response.optJSONArray("response");

            if(jPosts == null || jPosts.length() == 0) {
                Toast.makeText(context.getApplicationContext(), "No Posts Found!", Toast.LENGTH_LONG).show();
                return;
            }

            ArrayList<TechStreet.Post> posts = new ArrayList<>();

            for(int i = 0; i < jPosts.length(); i++) {
                TechStreet.Post post = new TechStreet.Post();

                JSONObject jPost = jPosts.getJSONObject(i);

                post.id = jPost.optInt("id");
                post.title = jPost.optString("title");
                post.url = jPost.optString("url");
                post.body = jPost.optString("body");
                post.username = jPost.optString("username");
                post.timestamp = jPost.optLong("timestamp", 0);
                post.lastUpdated = jPost.optString("lastUpdated");
                post.votes = jPost.optInt("votes");
                post.comments = jPost.optInt("comments");

                posts.add(post);
            }

            ListView list = (ListView) findViewById(R.id.list);

            list.setAdapter(new PostsAdapter(context, posts));
        }
    }

    class PostsAdapter extends ArrayAdapter<TechStreet.Post> {

        Activity context;
        ArrayList<TechStreet.Post> posts = null;

        public PostsAdapter(Activity context, ArrayList<TechStreet.Post> posts) {
            super(context, R.layout.post, posts);

            this.context = context;
            this.posts = posts;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null)
                convertView = context.getLayoutInflater().inflate(R.layout.post, parent, false);

            TechStreet.Post post = posts.get(position);
            ((TextView) convertView.findViewById(R.id.votes)).setText(post.votes.toString());
            ((TextView) convertView.findViewById(R.id.title)).setText(post.title);
            ((TextView) convertView.findViewById(R.id.username)).setText(post.username);

            String value = "";
            if(post.timestamp > 0)
                value = (String) DateUtils.getRelativeTimeSpanString(post.timestamp * 1000, System.currentTimeMillis(), 0);

            ((TextView) convertView.findViewById(R.id.timestamp)).setText(value);

            ((TextView) convertView.findViewById(R.id.comments)).setText(post.comments + " comments");

            return convertView;
        }
    }
}
