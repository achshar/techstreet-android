package com.achshar.techstreet;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

public class Profile extends TechStreet.Main {

    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences("authentication", 0);
        String username = preferences.getString("username", null);

        // If username wasn't found then go to login
        if(username == null) {
            Intent intent = new Intent(this, Login.class);
            intent.putExtra("InvalidToken", true);

            startActivity(intent);
            return;
        }

        setTitle(username + "'s Profile");

        HashMap<String, String> GETParams = new HashMap<>();
        GETParams.put("username", username);
        GETParams.put("action", "get");

        new API.Call("profile.php", GETParams, new HashMap<String, String>(), new userProfile(this));
    }

    class userProfile extends API.Callback {

        userProfile(Activity context) { super(context); }

        public void method(JSONObject response) throws JSONException {

            if(!response.optBoolean("status", false)) {
                Toast.makeText(context.getApplicationContext(), "Profile not found", Toast.LENGTH_LONG).show();
                return;
            }

            JSONArray profiles = response.optJSONArray("response");
            JSONObject profile = profiles.getJSONObject(0);

            String value;
            TextView view;

            value = profile.isNull("username") ? "-" : profile.optString("username");
            view = (TextView) findViewById(R.id.username);

            if(view != null)
                view.setText(value);

            value = profile.isNull("email") ? "-" : profile.optString("email");
            view = (TextView) findViewById(R.id.email);

            if(view != null)
                view.setText(value);

            value = profile.isNull("commentkarma") ? "0" : profile.optString("commentkarma");
            view = (TextView) findViewById(R.id.commentkarma);

            if(view != null)
                view.setText(value);

            value = profile.isNull("linkkarma") ? "0" : profile.optString("linkkarma");
            view = (TextView) findViewById(R.id.linkkarma);

            if(view != null)
                view.setText(value);

            value = profile.isNull("timestamp") ? "" : profile.optString("timestamp");
            view = (TextView) findViewById(R.id.membersince);

            if(view != null) {
                Long now = System.currentTimeMillis();
                Long then = Long.parseLong(value) * 1000;

                if(then > 0)
                    value = (String) DateUtils.getRelativeTimeSpanString(then, now, 0);

                view.setText(value);
            }

            value = profile.isNull("aboutme") ? "-" : profile.optString("aboutme");
            view = (TextView) findViewById(R.id.aboutme);

            if(view != null)
                view.setText(value);
        }
    }

}
