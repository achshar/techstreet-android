package com.achshar.techstreet;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Login extends AppCompatActivity {

    TextInputLayout usernameWrapper;
    TextInputLayout passwordWrapper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences("authentication", 0);
        if(!preferences.getString("token", "").isEmpty() && !getIntent().getBooleanExtra("InvalidToken", false)) {
            startActivity(new Intent(this, Posts.class));
            return;
        }

        setContentView(R.layout.activity_main);

        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        usernameWrapper.setHint(getResources().getString(R.string.mainLabelUsername));
        usernameWrapper.setErrorEnabled(true);

        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        passwordWrapper.setHint(getResources().getString(R.string.mainLabelPassword));
        passwordWrapper.setErrorEnabled(true);
    }

    public void formSubmitted(View view) {

        String username = usernameWrapper.getEditText().getText().toString();
        String password = passwordWrapper.getEditText().getText().toString();

        usernameWrapper.setError(null);
        passwordWrapper.setError(null);

        if(username.isEmpty() || password.isEmpty()) {

            if(username.isEmpty())
                usernameWrapper.setError("Username is required!");

            if(password.isEmpty())
                passwordWrapper.setError("Password is required!");

            return;
        }

        HashMap<String, String> GETParams = new HashMap<>();

        HashMap<String, String> POSTParams = new HashMap<>();
        POSTParams.put("username", username);
        POSTParams.put("password", password);

        if(view.getId() == R.id.authenticate) {

            SharedPreferences preferences = getSharedPreferences("authentication", 0);
            preferences.edit().putString("username", username).apply();

            GETParams.put("action", "login");
            new API.Call("user.php", GETParams, POSTParams, new LoginAttempted(this));

        } else if(view.getId() == R.id.register) {

            GETParams.put("action", "register");
            new API.Call("user.php", GETParams, POSTParams, new RegisterAttempted(this));

        }
    }

    class LoginAttempted extends API.Callback {

        LoginAttempted(Activity context) {
            super(context);
        }

        public void method(JSONObject response) throws JSONException {

            TextInputLayout usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
            TextInputLayout passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);

            if(!response.optBoolean("status", false)) {

                TextInputLayout ErrorWrapper = null;

                if(response.optString("response").equals("Invalid User!"))
                    ErrorWrapper = usernameWrapper;

                else if(response.optString("response").equals("Invalid Password!"))
                    ErrorWrapper = passwordWrapper;

                if(ErrorWrapper != null)
                    ErrorWrapper.setError(response.optString("response"));

            } else {

                SharedPreferences preferences = context.getSharedPreferences("authentication", 0);
                String token = response.getString("response");
                preferences.edit().putString("token", token).apply();

                Snackbar.make(context.findViewById(R.id.container), "Login Successful", Snackbar.LENGTH_LONG).show();

                context.startActivity(new Intent(context, Posts.class));
            }
        }
    }

    class RegisterAttempted extends API.Callback {

        RegisterAttempted(Activity context) {
            super(context);
        }

        public void method(JSONObject response) throws JSONException {

            if(!response.optBoolean("status", false)) {

                TextInputLayout ErrorWrapper = null;

                if(response.optString("response").equals("User Already Exists!"))
                    ErrorWrapper = usernameWrapper;

                if(ErrorWrapper != null)
                    ErrorWrapper.setError(response.optString("response"));

            } else {
                Snackbar.make(context.findViewById(R.id.container), "Registration Successful, you can log in now.", Snackbar.LENGTH_LONG).show();
            }
        }
    }
}