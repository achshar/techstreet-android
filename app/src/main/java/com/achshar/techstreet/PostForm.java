package com.achshar.techstreet;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PostForm extends TechStreet.Main {

    TextInputLayout titleWrapper;

    TextInputLayout urlWrapper;

    TextInputLayout bodyWrapper;

    private TechStreet.Post post = null;

    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_postform);

        titleWrapper = (TextInputLayout) findViewById(R.id.titleWrapper);
        titleWrapper.setHint(getResources().getString(R.string.postFormLabelTitle));
        titleWrapper.setErrorEnabled(true);

        urlWrapper = (TextInputLayout) findViewById(R.id.urlWrapper);
        urlWrapper.setHint(getResources().getString(R.string.postFormLabelUrl));
        urlWrapper.setErrorEnabled(true);

        bodyWrapper = (TextInputLayout) findViewById(R.id.bodyWrapper);
        bodyWrapper.setHint(getResources().getString(R.string.postFormLabelBody));
        bodyWrapper.setErrorEnabled(true);

        if(post == null) {
            setTitle("Add Post");
        } else {
            titleWrapper.getEditText().setText(post.title);
            urlWrapper.getEditText().setText(post.url);

            setTitle("Edit Post");
        }

        super.onCreate(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.postform, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                postSubmitted();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void postSubmitted() {

        String title = titleWrapper.getEditText().getText().toString();
        String url = urlWrapper.getEditText().getText().toString();
        String body = bodyWrapper.getEditText().getText().toString();

        if(title.isEmpty()) {
            titleWrapper.setError("Title is required");
            return;
        }

        if(url.isEmpty() && body.isEmpty()) {
            urlWrapper.setError("URL or body is required");
            bodyWrapper.setError("URL or body is required");
            return;
        }

        HashMap<String, String> POSTParams = new HashMap<>();
        POSTParams.put("title", title);
        POSTParams.put("url", url);
        POSTParams.put("body", body);

        HashMap<String, String> GETParams = new HashMap<>();
        if(post == null) {
            GETParams.put("action", "insert");
        } else {
            POSTParams.put("id", String.valueOf(post.id));
            GETParams.put("action", "update");
        }

        new API.Call("post.php", GETParams, POSTParams, new PostSubmitAttempted(this));
    }

    class PostSubmitAttempted extends API.Callback {

        PostSubmitAttempted(Activity context) {
            super(context);
        }

        public void method(JSONObject response) throws JSONException {

            if(!response.optBoolean("status", false)) {
                Toast.makeText(context.getApplicationContext(), "Error Submitting Post", Toast.LENGTH_LONG).show();
            } else {
                int id = response.getInt("response");
                Snackbar.make(context.findViewById(R.id.container), "New Post Added!", Snackbar.LENGTH_LONG).show();
            }
        }
    }
}
