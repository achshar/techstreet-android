package TechStreet;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Saurabh Arora on 16-03-2016.
 */
public class Post implements Serializable {

    public int id;

    public String title;

    public String url;

    public String body;

    public String username;

    public Long timestamp;

    public String lastUpdated;

    public Integer votes;

    public Integer vote;

    public Integer comments;
}
