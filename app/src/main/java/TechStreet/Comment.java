package TechStreet;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Saurabh Arora on 16-03-2016.
 */
public class Comment implements Serializable {

    public int id;

    public Date timestamp;

    public String username;

    public String body;

    public int parent;

    public int post;

    public Date lastUpdated;
}
