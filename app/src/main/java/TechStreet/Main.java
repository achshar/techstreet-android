package TechStreet;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.achshar.techstreet.Login;
import com.achshar.techstreet.Posts;
import com.achshar.techstreet.Profile;
import com.achshar.techstreet.R;
import com.achshar.techstreet.PostForm;
import com.achshar.techstreet.Settings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Achin on 2016-03-16.
 */
public class Main extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        String[] navigationDrawerList = new String[] {
                "Home", "Profile", "Add Post", "Settings", "Logout"
        };
        ListView navigationDrawer = (ListView) findViewById(R.id.navigation_drawer);

        navigationDrawer.setAdapter(new ArrayAdapter<>(this, R.layout.navigation_drawer_item, navigationDrawerList));

        navigationDrawer.setOnItemClickListener(new DrawerItemClickListener(this));
    }

    public void logout() {
        HashMap<String, String> GETParams = new HashMap<>();
        GETParams.put("action", "purge");

        HashMap<String, String> POSTParams = new HashMap<>();

        new API.Call("sessions.php", GETParams, POSTParams, new LogoutDone(this));

        SharedPreferences preferences = this.getSharedPreferences("authentication", 0);
        preferences.edit().clear().apply();

        startActivity(new Intent(this, Login.class));
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        Activity context;

        public DrawerItemClickListener(Activity context) {
            this.context = context;
        }

        public void onItemClick(AdapterView parent, View view, int position, long id) {

            switch(((TextView) view).getText().toString()) {
                case "Home":
                    context.startActivity(new Intent(context, Posts.class));
                    break;
                case "Profile":
                    context.startActivity(new Intent(context, Profile.class));
                    break;
                case "Add Post":
                    context.startActivity(new Intent(context, PostForm.class));
                    break;
                case "Settings":
                    context.startActivity(new Intent(context, Settings.class));
                    break;
                case "Logout":
                    logout();
                    break;
            }
        }
    }

    class LogoutDone extends API.Callback {

        LogoutDone(Activity context) { super(context); }

        public void method(JSONObject response) throws JSONException {

            Toast.makeText(context.getApplicationContext(), "Successfully Logged Out", Toast.LENGTH_LONG).show();
        }
    }
}
