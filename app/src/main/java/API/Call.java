package API;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.achshar.techstreet.Login;
import com.achshar.techstreet.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class Call extends AsyncTask<String, Void, String> {

    private static String token = null;

    private String url;

    private HashMap<String, String> GETParams;

    private HashMap<String, String> POSTParams;

    private API.Callback callback;

    public Call(String url, HashMap<String, String> GETParams, HashMap<String, String> POSTParams, API.Callback callback) {
        this.url = url;
        this.GETParams = GETParams;
        this.POSTParams = POSTParams;
        this.callback = callback;

        if(!this.url.equals("users.php")) {
            SharedPreferences preferences = this.callback.context.getSharedPreferences("authentication", 0);
            Call.token = preferences.getString("token", null);
        }

        this.execute();
    }

    protected String doInBackground(String... params) {

        callback.context.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(callback.context.getApplicationContext(), "Loading", Toast.LENGTH_LONG).show();
            }
        });

        HttpURLConnection connection = null;
        String APIRoot = this.callback.context.getResources().getString(R.string.APIRoot);

        if(Call.token != null)
            GETParams.put("token", Call.token);

        try {
            String url = APIRoot + this.url + "?" + makeQueryString(GETParams);

            connection = (HttpURLConnection) new URL(url).openConnection();

            if(!POSTParams.isEmpty()) {
                byte[] postDataBytes = makeQueryString(POSTParams).getBytes("UTF-8");

                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                connection.setDoOutput(true);
                connection.getOutputStream().write(postDataBytes);
            }

            if(connection.getResponseCode() == 401 && !this.url.equals("user.php")) {
                goToLogin();
            } else {
                return readStream(new BufferedInputStream(connection.getInputStream()));
            }
        } catch (Exception e) {
            callback.context.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(callback.context.getApplicationContext(), "Error Establishing Connection", Toast.LENGTH_LONG).show();
                }
            });
        } finally {
            if(connection != null)
                connection.disconnect();
        }

        return "{}";
    }

    protected void onPostExecute(String response) {

        String log = "";
        log += "\nURL:      " + url;
        log += "\nGET:      " + GETParams.toString();
        log += "\nPOST:     " + POSTParams.toString();
        log += "\nResponse: " + response;

        Log.d("APICall", log);

        try {
            JSONObject json = new JSONObject(response);

            if(json.length() > 0)
                callback.method(json);

        } catch (JSONException e) {
            Log.d("APICall JSON Exception", e.toString());
        }
    }

    private String makeQueryString(HashMap<String, String> map) throws UnsupportedEncodingException {
        StringBuilder string = new StringBuilder();
        for(HashMap.Entry param : map.entrySet()) {
            if(string.length() != 0)
                string.append('&');

            string.append(URLEncoder.encode((String) param.getKey(), "UTF-8"));
            string.append('=');
            string.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }

        return string.toString();
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for(String line = r.readLine(); line != null; line = r.readLine()){
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

    private void goToLogin() {
        callback.context.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(callback.context.getApplicationContext(), "Please login again", Toast.LENGTH_LONG).show();
            }
        });

        Intent intent = new Intent(callback.context, Login.class);
        intent.putExtra("InvalidToken", true);

        callback.context.startActivity(intent);
    }
}
