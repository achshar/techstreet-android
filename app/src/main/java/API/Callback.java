package API;

import android.app.Activity;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class Callback {

    public Activity context = null;

    public Callback(Activity context) {
        this.context = context;
    }

    public abstract void method(JSONObject response) throws JSONException;
}
